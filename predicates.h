#pragma once

#include <algorithm>
#include <functional>

namespace std 
{
	auto even = [](int n) { return (n % 2) == 0; };

	static bool canDivide(int n, int divider)
	{
		return (n % divider) == 0;
	}
	auto divBy5 = std::bind(canDivide, std::placeholders::_1, 5);

	struct natural
	{
		bool operator()(int n) const
		{
			return n > 0;
		}
	};
}