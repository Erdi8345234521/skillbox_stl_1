// Skillbox_STL.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "all_of.h"
#include "predicates.h"
#include <iostream>
#include <map>
#include <unordered_map>
#include <set>

using namespace std;

struct BaseContainer 
{
public:
	//������� �������������� ����������
	virtual void AutoFillContainer() {}

	//������� ��� ������ ������������� ���������� �� �����
	virtual void Print() 
	{
		cout << "Print value container " << NameContaner << ": " << endl;
	}

protected:
	string NameContaner;
};

struct UnorderedMapContainer : public BaseContainer
{

public:

	UnorderedMapContainer()
	{
		//Default Constructor
		NameContaner = "UnorderedMapContainer";
	}

	int Count_If(string FindValue) 
	{
		auto IsNubmer = [FindValue](std::pair <int, string> Test)
		{
			if (Test.second == FindValue)
			{
				return true;
			}
			return false;
		};

		return count_if(stringToInt.begin(), stringToInt.end(), IsNubmer);
	}

	virtual void AutoFillContainer() override
	{
		stringToInt[1] = "One";
		stringToInt[2] = "Two";
		stringToInt[3] = "Three";
		stringToInt[4] = "four";
		stringToInt[5] = "four";
		stringToInt[6] = "four";
	}

	virtual void Print() override 
	{
		BaseContainer::Print();
		for (const auto& n : stringToInt)
		{
			cout << "Key: " << n.first << " Value: " << n.second << endl;
		}
	}

private:
	std::unordered_map<int, string> stringToInt;
};

struct MapContainer : BaseContainer 
{
public:

	MapContainer()
	{
		//Default Constructor
		NameContaner = "MapContainer";
	}

	string FindValueByKey(int Key)
	{
		return mymap.find(Key)->second;
	}

	virtual void AutoFillContainer() 
	{
		mymap[1] = "One";
		mymap[2] = "Two";
		mymap[3] = "Three";
	}

	virtual void Print() override
	{
		BaseContainer::Print();
		
		for (const auto& n : mymap)
		{
			cout << "Key: " << n.first << " Value: " << n.second << endl;
		}
	}

private:
	std::map<int, string> mymap;
};

struct SetContainer : BaseContainer 
{
public:
	SetContainer()
	{
		//Default Constructor
		NameContaner = "SetContainer";
	}

	virtual void AutoFillContainer() override
	{
		TestSet.insert("first");
		TestSet.insert("second");
		TestSet.insert("third");
	}

	virtual void Print() override
	{
		BaseContainer::Print();

		auto printLambda = [](string Set)
		{
			cout << Set << endl;
		};

		for_each(TestSet.begin(), TestSet.cend(), printLambda);
	}

private:

	set<std::string> TestSet;
};

int main()
{
	setlocale(LC_ALL, "Russian");

	//find and map-------------------------------------------------------

	MapContainer TestMapContainer;
	TestMapContainer.AutoFillContainer();
	TestMapContainer.Print();

	cout << "Value find: " << TestMapContainer.FindValueByKey(2) << endl;


	//Count if and UnorderedMapContainer --------------------------------

	UnorderedMapContainer TestContainer;

	//��������� ��������� ����������
	TestContainer.AutoFillContainer();

	//������� ���������� ������
	TestContainer.Print();

	//�������, ���-�� ����������� �������� "Four"
	std::cout << "���-�� ���������� �������� Four: " << TestContainer.Count_If("four") << endl;


	//for_each and set------------------------------------------------

	SetContainer TestSetContainer;
	TestSetContainer.AutoFillContainer();
	//������� for_each ������������ � Print
	TestSetContainer.Print();
}